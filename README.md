# README #
DeepSeq Simulator version 1.0

October 31, 2017

Developed by Hadar Isaac (hisaac@imagenix.com), Imagenix Corp.
Under the supervision of Francisco De La Vega (delavefm@gmail,com), 
TOMA Biosciences, Inc.

(c) 2017 TOMA Biosciences Inc., all rights reserved.
Permission to use, copy, modify, and distribute this software 
and its documentation for any purpose and without fee is hereby 
granted, provided that the above copyright notice appears in all 
copies and that both that copyright notice and this permission 
notice appear in supporting documentation. 

YOU ARE SOLELY RESPONSIBLE FOR ANY DAMAGE OR LOSS OF USE OF YOUR 
DATA. THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS. WE EXCLUDE 
ALL LIABILITY FOR DAMAGES RELATED TO CORRUPTION OR LOSS OF DATA. 
ALL ANY IMPLIED GUARANTEE, CONDITION OR WARRANTY BY TOMA AND/OR 
IT�S LICENSORS TO YOU INCLUDING THOSE AS TO THEIR NON-
INFRINGEMENT OF THIRD PARTY RIGHTS, QUALITY, FITNESS FOR PURPOSE, 
ACCURACY OR OPERABILITY IS EXCLUDED TO THE MAXIMUM EXTENT 
PERMITTED BY LAW. NEITHER TOMA OR ITS LICENSORS WILL BE LIABLE 
TO YOU FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL OR INCIDENTAL 
LOSS OR DAMAGE DUE TO LOSS OR CORRUPTION OF DATA OR THAT ARISE 
UNDER OR IN CONNECTION WITH THE USE OF THIS SOFTWARE. 

### What is this software for? ###

DeepSeq Simulator is a software tool to simulate a sequencing 
experiment to identify sequence variants in targeted panels and 
estimate the accuracy of finding such variants depending on 
common experimental parameters. The main application is to
simulate the situation of detection of cancer somatic mutations
in sequencing data from tumor tissues, focusing on the situation
at the limit of detection. 

### How do I get set up? ###

DeepSeq Simulator is a JavaScript code embedded in a web page. 
This can be deployed in a website, or open directly with a modern 
web browser. Chrome is recommended for Windows and MacOS, but also
works well with Firefox and Safari in MacOS.

### How do I operate it? ###

DeepSeq Simulator is operated by entering the desired parameters and
observing how the values change interactively. Please read the
manual (in development) for more details.

